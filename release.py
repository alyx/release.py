#!/usr/bin/env python3

# curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: gDybLx3yrUK_HLp3qPjS" \
#      --data '{ "name": "New release", "tag_name": "v0.3", "description": "Super nice release", "assets": { "links": [{ "name": "hoge", "url": "https://google.com" }] } }' \
#      --request POST http://localhost:3000/api/v4/projects/24/releases

import sys, frontmatter, requests, json, os

def main():
  data = {}

  if len(sys.argv) < 2:
    print("Syntax: release.py <input file>")
    exit(1)

  post = frontmatter.load(sys.argv[1])

  data["name"] = post["name"]
  data["tag_name"] = post["tag"]
  data["description"] = post.content

  print(data)

  r = requests.post("{}/api/v4/projects/{}/releases".format(post["domain"], post["id"]), data=data, headers={"PRIVATE-TOKEN": os.environ["GITLAB_TOKEN"]})
  if r.status_code == requests.codes.ok:
    print("Release posted successfully\n")
  else:
    print("Release posting failed")
    print(r.text)


if __name__ == "__main__":
  main()
