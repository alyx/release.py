# release.py

This script generates and uploads releases to `Gitlab` using a Markdown document containing your release information.

## Installation

`pip install frontmatter requests`

## Usage

- Define the environmental variable `GITLAB_TOKEN` to a personal access token generated at `$YOUR_GITLAB_URL//profile/personal_access_tokens`
- Write a markdown file structured like the [example](#example)
- run `./release.py <file>`

## Example

```
---
name: gcompat 0.4.0 - Grandiloquent Gardevoir
tag: 0.4.0
domain: https:/code.foxkit.us
id: 148
---
# gcompat's exciting new release

Any content past the second `---` is interpreted as Github-formatted Markdown, like everything else used on Gitlab.
Any content here will be part of the description for your release.
```


